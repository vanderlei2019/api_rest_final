const mysql = require('mysql')
//CONST: declaração de variaveis de scopo bloco 
const conexao = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'admin',
    database: 'agenda-petshop'
})

module.exports = conexao

//conexão como o banco de dados e criação de da database e usuario e senha
