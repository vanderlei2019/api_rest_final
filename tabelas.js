class Tabelas{
    init(conexao){ // conexão com banco de dados
        this.conexao = conexao

        this.criarAtendimento()

    }

    criarAtendimento() { //Criação tabela atendimento e seus atributos
        const sql = `CREATE TEABLE IF NOT EXISTS Atendimentos
        (id int NOT NULL AUTO_INCREMENT, 
        cliente VARCHAR(50) NOT NULL, 
        pet VARCHAR(20), 
        servico VARCHAR(20) NOT NULL,
        status VARCHAR(20) NOT NULL,
        observacoes text,
        PRIMARY KEY(id))`

        this.conexao.query(sql, erro => {
            if(erro) {
            console.log(erro)
            }else{
            console.log('Tabela Atendimentos criada com sucesso')
            }

        })


    }
    

}

module.exports = new Tabelas


/*Ocorre de cria as tabelas para o database com seus parametros 
acontece tambem a parte de popular as tabelas */
 