const customExpress = require('./config/customExpress')
const conexao = require('./infraestrutura/conexao')
const tabelas = require('./infraestrutura/tabelas')
//CONST: declaração de variaveis de scopo bloco 
conexao.connect (erro => { //Loop: erro não conseguiu de conexão com servidor
    if (erro){
        console.log(erro)
    }else{                  // Loop: conseguiu conexação
        console.log('Servidor conectado com sucesso')
        
        tabelas.init(conexao)

        const app = customExpress() // 

        app.listen(3000, () => console.log('servidor rodando na porta 3000'))

    }
    

})
/* Ocorre a conexao com servidor atarvez da defenição da porta 3000 */
