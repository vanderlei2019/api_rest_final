const express = require('express')
const consign = require('consign')
const bodyParser = require('body-parser')
 //CONST: declaração de variaveis de scopo bloco 
module.exports = () => {
 const app = express()
 
 app.use(bodyParser.json())
 app.use(bodyParser.urlencoded({ extended: true }))
 
 consign()
   .include('controllers')
   .into(app)
 
 return app
}

//ligação do objeto app com o modulo contorllers